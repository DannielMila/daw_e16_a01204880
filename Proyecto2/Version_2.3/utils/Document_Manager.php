<?php

class Document_Manager 
{
    public $errores = array(
	-1 => 'No se pudo cargar el documeno', 
	-2 => 'Extension invalida', 
	-3 => 'Archivo incompatible', 
	-4 => 'El archivo ya existe',
    -5 => 'No se encontro el archivo',
    -6 => 'Error al subir imagen'
	);
    
    private $extensiones_Validas = array("pdf","doc","docx");
    
    private function detectFileMimeType($filename='')
    {
        $filename = escapeshellcmd($filename);
        $command = "file -b --mime-type -m /usr/share/misc/magic {$filename}";

        $mimeType = shell_exec($command);

        return trim($mimeType);
    }
    
    public function subir_Documento($imagen)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];
        
        $temp = explode(".", $imagen["name"]);
        $extension = end($temp);
        if($image["error"]>0)
		{
			return -1;
		}
        if(in_array($extension,$extensiones_Validas))
		{
			return -2;
		}
        $mime = $this->detectFileMimeType($imagen['tmp_name']);
        switch ($mime) {
            case 'application/msword':
            case 'application/ms-word':
            case 'text/pdf':
            case 'application/pdf':
            case 'application/x-pdf':
            case 'application/acrobat':
            case 'applications/vnd.pdf':
            case 'application/zip':
            case 'text/x-pdf':
                break;
           default:
               return -3;
        }
        $nombreEnServidor = time() . rand() . "." . $extension;
        if (file_exists($root."/uploads/documentos/" . $nombreEnServidor))
        {
            return -4;
        }
        $ruta = "/uploads/documentos/" . $nombreEnServidor;
        $res = move_uploaded_file($imagen["tmp_name"] , $root.$ruta);
        if(!$res)
        {
            return -6;
        }
        return $ruta;
    }
    
    public function obtener_Nombre($imagen)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];

        
        $temp = explode(".", $imagen["name"]);
        $extension = end($temp);
        return $imagen["name"];
    }
    
    public function eliminar_Documento($src)
    {
        if (file_exists($src)) 
        {
            unlink($filename);
            return 1;
        } else 
        {
            return -5;
        }
    }
    
    public function error($errorNum)
    {
        if (array_key_exists($errorNum, $errores)) 
        {
            return "ERROR " . $errorNum . ":" .$errores[$errorNum];
        }
        return "ERROR INVALIDO";
    }
}

?>