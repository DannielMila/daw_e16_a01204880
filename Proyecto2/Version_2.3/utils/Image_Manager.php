<?php

class Image_Manager
{
    private $errores = array(
	-1 => 'No se pudo cargar la imagen', 
	-2 => 'Extension invalida', 
	-3 => 'Archivo incompatible', 
	-4 => 'El archivo ya existe',
    -5 => 'No se encontro el archivo',
    -6 => 'Error al subir imagen'
	);
    
    private $extensiones_Validas = array("jpeg", "jpg", "png");
    
    public function subirImagen($imagen)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];
        
        $temp = explode(".", $imagen["name"]);
        $extension = end($temp);
        if($imagen["error"]>0)
		{
			return -1;
		}
        if(in_array($extension,$extensiones_Validas))
		{
			return -2;
		}
        if (($imagen["type"] == "image/gif")
		|| ($imagen["type"] == "image/jpeg")
		|| ($imagen["type"] == "image/jpg")
		|| ($imagen["type"] == "image/pjpeg")
		|| ($imagen["type"] == "image/x-png")
		|| ($imagen["type"] == "image/png"))
		{
            $nombreEnServidor = time() . rand() . "." . $extension;
			if (file_exists($root."/uploads/imagenes/" . $nombreEnServidor))
			{
				return -4;
			}
            $ruta = "/uploads/imagenes/" . $nombreEnServidor;
			$res = move_uploaded_file($imagen["tmp_name"] , $root.$ruta);
            if(!$res)
            {
                return -6;
            }
			return $ruta;
        }else
        {
            return -3;
        }
    }
    
    public function eliminar_Imagen($src)
    {
        if (file_exists($src)) 
        {
            unlink($filename);
            return 1;
        } else 
        {
            return -5;
        }
    }
    
    public function error($errorNum)
    {
        if (array_key_exists($errorNum, $errores)) 
        {
            return "ERROR " . $errorNum . ":" .$errores[$errorNum];
        }
        return "ERROR INVALIDO";
    }
}

?>