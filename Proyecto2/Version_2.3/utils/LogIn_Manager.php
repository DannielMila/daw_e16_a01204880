<?php
require_once('info.php');
require_once('Database_Manager.php');

class Login_Manager
{
    
	public $loggedIn = false;
	public $user = false;
    
    private $initCalled	= false;
    private $db_Manager;
	private $cookie;
	private $session;
	private $db_Enable = true;
    private $remCook;
    
    public function __construct()
	{

		if(Info::$phpSessionStart == true)
        {
			@session_start();
		}
		
        $this->db_Manager = new Database_Manager( Info::$host_nombre, Info::$db_usuario, Info::$db_pass );
        
        
        
		$this->db_Manager->conectarConDB( Info::$db_nombre );
                
        if($this->db_Manager->getEstado() != 2)
        {
            $this->db_Enable = false;
        }
        
        
		$this->cookie	= isset($_COOKIE['logSyslogin']) ? $_COOKIE['logSyslogin'] : false;
		$this->session  = isset($_SESSION['logSyscuruser']) ? $_SESSION['logSyscuruser'] : false;
        $this->remCook  = isset($_COOKIE['logSysrememberMe']) ? $_COOKIE['logSysrememberMe'] : false;


        $key = Info::$cookieKey;
		$encUserID 		= hash("sha256", "{$key}{$this->session}{$key}");
        
        $this->loggedIn = $this->cookie == $encUserID ? true : false;
        
        
        if($this->rememberMe === true && isset($this->remCook) && $this->loggedIn === false)
        {
				
				$encUserID	= hash("sha256", "{$key}{$this->remCook}{$key}");
            
				$this->loggedIn = $this->cookie == $encUserID ? true : false;
				
				if($this->loggedIn === true)
                {
					$_SESSION['logSyscuruser'] = $this->remCook;
				}
        }
        		
        $this->user = $this->session;
		return true;
	}
    
    public function init() 
	{
		if( $this->loggedIn && array_search($this->curPage(), Info::$staticPages) === true )
        {
			$this->redirect($Info::$homePage);
		}else if(!$this->loggedIn && array_search($this->curPage(), Info::$staticPages) === false )
        {
			$this->redirect(Info::$loginPage);
		}
		$this->initCalled = true;

	}
    
    public function login($username, $password, $cookies = true)
	{
		if($this->db_Enable == true)
        {
            // Checar si el usuario existe
			$value = array(
				Info::$field_usuario => $username
			);
			$result =$this->db_Manager->obtenerArregloDeQuery(Info::$nombre_tabla_usuarios,$value);
			if($result < 0)
			{
				return false;
			}

			$us_nick = $result[0][Info::$field_usuario];
			$us_pass = $result[0][Info::$field_contraseña];
			$us_salt = $result[0][Info::$field_salt];
            $us_stat = $result[0][Info::$field_estado];
            $us_id = $result[0][Info::$field_id];
            $salt = Info::$password_Salt;
            $saltedPass = hash('sha256', "{$password}{$salt}{$us_salt}");
            // Checar si el usuario esta bloqueado
            if(substr($us_stat, 0, 2) == "b-")
            {
                $blockedTime = substr($us_stat, 2);
                if(time() < $blockedTime){
                    $block = true;
                    return array(
                        "status" 	=> "blocked",
                        "minutes"	=> round(abs($blockedTime - time()) / 60, 0),
                        "seconds"	=> round(abs($blockedTime - time()) / 60*60, 2)
                    );
                }else
                {
                    $this->db_Manager->actualizarEnTabla(Info::$nombre_tabla_usuarios,array(Info::$field_estado => ''),$value);
                }
            }
            // Checar si esta bloqueado
            if(!isset($block) && $saltedPass == $us_pass)
            {
                if($cookies === true)
                {
                    $_SESSION['logSyscuruser'] = $us_id;
                    setcookie("logSyslogin", hash("sha256", Info::$cookieKey.$us_id.Info::$cookieKey), time()+3600*99*500, "/");
                    
                    if( isset($_POST['remember_me']) && $this->rememberMe == true )
                    {
							setcookie("logSysrememberMe", $us_id, time()+3600*99*500, "/");
				    }

                    $this->loggedIn = true;

                    $this->db_Manager->actualizarEnTabla(Info::$nombre_tabla_usuarios,array(
                        Info::$field_estado => '',
                        Info::$field_ultimo_conectado => $nowFormat = date('Y-m-d'),
                    ),$value);
                    // Redirect
                    if( $this->initCalled)
                    {
                        $this->redirect(Info::$homePage);
                    }
                }
                return true;
            }else
            {
                // Incorrect password
                if(Info::$blockBruteForce === true)
                {
                    // Checking for brute force is enabled
                    if($us_stat == "")
                    {
                        $this->db_Manager->actualizarEnTabla(Info::$nombre_tabla_usuarios,array(Info::$field_estado => "1"),$value);
                    }else if($us_stat == 5)
                    {
                        $aux = Info::$bf_time;
                        $this->db_Manager->actualizarEnTabla(Info::$nombre_tabla_usuarios,array(Info::$field_estado => "b-" . strtotime("+{$aux} seconds", time())),$value);
                    }else if(substr($status, 0, 2) == "b-")
                    {
							// Account blocked!!!!
                    }else if($us_stat < 5)
                    {
                        $aux = $us_stat +1;
                        $this->db_Manager->actualizarEnTabla(Info::$nombre_tabla_usuarios,array(Info::$field_estado => ($us_stat + 1)),$value);
                    }
                }
                return false;
            }
		}
    }
    
    public function register($username,$password,/*array*/ $others = "")
	{
		if($this->db_Enable == true)
		{
			$values = array(
				    Info::$field_usuario => $username
			         );
            
			$result=$this->db_Manager->verificarSiExiste(Info::$nombre_tabla_usuarios,$values);
            
                
			if($result < 0)
			{
				$us_salt	= $this->rand_string(30);
                $salt = Info::$password_Salt;
                $us_pass = hash('sha256', "{$password}{$salt}{$us_salt}");
				$nuevoElemento = array(
					Info::$field_usuario => $username,
					Info::$field_contraseña => $us_pass,
                    Info::$field_salt => $us_salt
				);
                if( count($others) > 0 )
                {
                    $array = array_merge($nuevoElemento, $others);
                }else
                {
                    $array = $nuevoElemento;
                }
				$this->db_Manager->insertarEnTabla(Info::$nombre_tabla_usuarios,$array);
                return 1;
			}
		}
        return -1;
	}
    
    public function logout()
    {
		session_destroy();
		setcookie("logSyslogin", "", time()-3600, "/");
		$this->redirect(Info::$loginPage);
		return true;
	}
    
    
            
    
    public function validEmail($email = "")
    {
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	
	public function rand_string($length) {
		$str="";
		$chars = "nsnZ1MuDv3ouz4AOntrJr3XglbEEdGWgRlh25tCeB1LJ9kbPW3V1dkWeAUGR8VgeMIhOIEpZmJ8PNE3jAR7iMSP5TLyBpLRVaQ9xFS1pKkxLen7qYemXzlG8Umb8pMPFatdnnA9tSzCx";
		$size = strlen($chars);
		for($i = 0;$i < $length;$i++) {
			$str .= $chars[rand(0,$size-1)];
		}
		return $str;
	}

	public function curPage(){
		$parts = parse_url($this->curPageURL());
		return $parts["path"];
	}
    
    public function curPageURL() {
		$pageURL = 'http';
		if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on"){$pageURL .= "s";}
		$pageURL .= "://";
		if($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}else{
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
    
    public function sendMail($email, $subject, $body)
    {
		mail($email, $subject, $body);
	}
    
    public function redirect($url, $status=302)
    {
		header("Location: $url", true, $status);
	}
}

?>