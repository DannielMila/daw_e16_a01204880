<?php
    require_once("Database_Manager.php");
    require_once("Info.php");

    $db = new Database_Manager(Info::$host_nombre, Info::$db_usuario, Info::$db_pass);

    $db->conectarConDB(Info::$db_nombre);
?>