//1. Entrada: un número pedido con un prompt. Salida: Una tabla con los números del 1 al número dado con sus cuadrados y cubos. Utiliza document.write para producir la salida 

document.write("<h2>Ejercicio 1</h2>");

var size = window.prompt("Problema #1:    Ingresar un numero: ");
var i, x;

document.write("<strong>  TABLA</strong><br><table><tr><th>^1</th><th>^2</th><th>^3</th></tr></strong>");

for (i = 0; i < size; i++) {
        x=i+1;
        document.write("<tr><td>" + x + "</td>");
        document.write("<td>" + Math.pow(x,2) + "</td>");
        document.write("<td>" + Math.pow(x,3) + "</td></tr>");
}

document.write("</table><br>");


//2 Entrada: Usando un prompt se pide el resultado de la suma de 2 números generados de manera aleatoria. Salida: La página debe indicar si el resultado fue correcto o incorrecto, y el tiempo que tardó el usuario en escribir la respuesta.

    N1 = Math.floor(Math.random()*100);
    N2 = Math.floor(Math.random()*100);
   
var startTime = Date.now(); //Start timer
var res = window.prompt("Problema #2 Ingresa el resultado de " + N1 + " + " + N2 +" ");
var stopTime = Date.now(); //Stop Timer


document.write("<h2>Problrma #2 </h2>");

document.write("<strong>Suma</strong><br>   " + N1 + " + " + N2 + " = " + res);
if (res== (N1+N2)) {
        
        document.write("<br> CORRECTO");     
} else {
        
        document.write("<br>INCORRECTO"); 
}

document.write("<br>Tiempo: " + ((stopTime - startTime)/1000) + " seg<br>");




//Función: contador. Parámetros: Un arreglo de números. Regresa: La cantidad de números negativos en el arreglo, la cantidad de 0's, y la cantidad de valores mayores a 0 en el arreglo.

document.write("<h2>Problema #3</h2>");

var arr = new Array(30);
var ceros=0, negativos=0, positivos=0;

for (i=0;i < arr.length;i++){
        arr[i] = Math.floor((Math.random() * 10) - 5);        
}

for (i=0;i < arr.length;i++){
        if (arr[i]==0) {
                ceros++;          
        } else if (arr[i]>0){
                positivos++;
        } else {
                negativos++;
        }
}

document.write("<br><strong>Contador</strong><br>");
document.write("Arreglo = [");
for (i=0;i < arr.length-1;i++){
        document.write(arr[i] + ", ");       
}
document.write(arr[arr.length-1] + "]<br>");
document.write("<br>Ceros: " + ceros + "");
document.write("<br>Negativos: " + negativos + "");
document.write("<br>Positivos: " + positivos + "");




//4 Función: promedios. Parámetros: Un arreglo de arreglos de números. Regresa: Un arreglo con los promedios de cada uno de los renglones de la matriz.

document.write("<h2>Problema #4</h2>");

var matrix = [];
var j;
var promedio=0;
document.write("<br>");
document.write("<strong>Promedio</strong><br>");
for(i = 0; i < 10; i++){
    matrix[i] = [];
    document.write("Fila " + i +" --| ");
    for(j = 0; j < 9; j++){ 
        matrix[i][j] = Math.floor((Math.random() * 10));
        promedio = matrix[i][j] + promedio;
        document.write(matrix[i][j] + " | ");
    }   
   matrix[i][j] = Math.floor((Math.random() * 10));
   promedio = matrix[i][j] + promedio;
   document.write(matrix[i][j] + "|  Promedio = " + (promedio/10) + " <br>");
    promedio=0;
}


//5 Función: inverso. Parámetros: Un número. Regresa: El número con sus dígitos en orden inverso.

document.write("<h2>Problema #5</h2>");

var inver = window.prompt("Ingresa el numero para invertir: ");
document.write("<strong>Inverso</strong><br>");

document.write("Normal: " + inver + "<br>");
document.write("Inverso:");
while(inver!=0){
       document.write(inver%10);
       inver = (inver - inver%10)/10;
}

//6 Crea una solución para un problema de tu elección (puede ser algo relacionado con tus intereses, alguna problemática que hayas identificado en algún ámbito, un problema de programación que hayas resuelto en otro lenguaje, un problema de la ACM, entre otros). El problema debe estar descrito en un documento HTML, y la solución implementada en JavaScript, utilizando al menos la creación de un objeto, y el objeto además de su constructor debe tener al menos 2 métodos. Muestra los resultados en el documento HTML.

document.write("<h2>#PREGUNTAS#</h2>");


document.write("<strong>Que diferencias y semejanzas hay entre Java y JavaScript?</strong>");

document.write("<br>Javascript se ejecuta en el navegador, mientras que Java no lo hace. Ambos lenguajes fueron creados dirigidos a Objetos Orientados");

document.write("<br><br><strong>Que metodos tiene el objeto Date?</strong>");

document.write("<br> Date.prototype.getDay(): Regresa el dia de la semana ");
document.write("<br> Date.prototype.getSeconds(): Regresa los segundo de la hora local");
document.write("<br> Date.prototype.getMonth() Regresa el mes de acuerdo a la fecha local");
document.write("<br>Date.prototype.getMinutes(): Regresa los minutos de la hora local");


document.write("<br><br><strong>Que metodos tienen los arreglos?</strong> ");

document.write("<br>Push: agrega nuevos elementos al arreglo");
document.write("<br>Pop: remueve el ultimo elemento del arrglo");
document.write("<br>Join: ingresa todos los elementos del arreglo a un string");
document.write("<br>Valueof: regresa el arreglo como un string");
document.write("<br>Shift: remueve el primer elemento del arreglo y cambia la posicion de todos uno hacia abajo");

document.write("<br><br><strong>Como se declara una variable con alcance local dentro de una funcion? </strong>");

document.write("<br> var = Nombre_Variable; dentro de una funcion");

document.write("<br><br><strong>Que implicaciones tiene utilizar variables globales dentro de funciones? </strong>");

document.write("<br> Se podría perder el control de las variables, por ejemplo volver a repetir nombre de Variables si es que se repiten en varias funciones ");

document.write("<br><br><strong>Que metodo de String se puede utilizar para buscar patrones con expresiones regulares? Para que podrias utilizar esto en una aplicación web? </strong>");

document.write("<br> exec: Busca el string deseado y regresa si encontro algo o no, y se puede utilizar en strings muy largos");