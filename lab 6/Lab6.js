///////////////////////////////////////////////
//onClick Events for TEXT
//////////////////////////////////////////////
var textSize = 4;
var str = document.getElementById("pInfo").textContent;
document.getElementById("pInfo").innerHTML = str.fontsize(textSize);



function fontBold(){
    document.getElementById("pInfo").style.fontWeight = "900";
}

function fontItalic(){
    document.getElementById("pInfo").style.fontStyle = "italic";
}

function fontNormal(){
    document.getElementById("pInfo").removeAttribute("style");
}

function fontPlus(){
    if (textSize < 10){
        textSize ++;
    }
    document.getElementById("pInfo").innerHTML = str.fontsize(textSize);
}

function fontMinus(){
        if (textSize > 1){
        textSize--;
    }
    document.getElementById("pInfo").innerHTML = str.fontsize(textSize);
}

///////////////////////////////////////
//onMouseOver extra info Event
//////////////////////////////////////
function extraInfo() {
    var newInf = "Danniel Mila<br>" +
                 "Tx Qrock<br>";
                 
    document.getElementById("extInf").innerHTML = newInf;
}

function noExtraInfo() {
    var newInf = "";
    document.getElementById("extInf").innerHTML = newInf;
}




function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}