
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";



CREATE TABLE IF NOT EXISTS `fruits` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Unidades` int(11) NOT NULL,
  `Cantidad` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Precio` int(11) NOT NULL,
  `Pais` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `fruits` (`id`, `Nombre`, `Unidades`, `Cantidad`, `Precio`, `Pais`) VALUES
(0, 'Manzana', 2, 'Kg', 50, 'Mexico'),
(1, 'Mango', 50, 'Kg', 79, 'Alemania'),
(2, 'Aguacate', 70, 'Kg', 15, 'Francia'),
(2, 'Aguacate', 70, 'Kg', 15, 'Francia');
